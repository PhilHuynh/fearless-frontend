
window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/states/';
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    const stateList = document.querySelector('#state')
    for (let state of data.states) {
      for (let name in state) {
        const listItem = document.createElement('option')
        listItem.setAttribute('value', state[name])
        listItem.innerHTML = name
        stateList.append(listItem)
      }
    }
  }
  const formTag = document.getElementById('create-location-form');
  formTag.addEventListener('submit', async(event) => {
    event.preventDefault();
    const formData = new FormData(formTag)
    const json = JSON.stringify(Object.fromEntries(formData))

    const locationUrl = 'http://localhost:8000/api/locations/'
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      }
    }
    const response = await fetch(locationUrl, fetchConfig)
    console.log("🚀 ~ file: new-location.js ~ line 34 ~ formTag.addEventListener ~ response", response)
    if(response.ok) {
      formTag.reset()
      const newLocation = await response.json();
      console.log("🚀 ~ file: new-location.js ~ line 37 ~ formTag.addEventListener ~ newLocation", newLocation)
    }
  });
});
